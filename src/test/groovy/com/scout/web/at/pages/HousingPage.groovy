package com.scout.web.at.pages


import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selenide.$
import static com.codeborne.selenide.Selenide.$$

class HousingPage extends Page {

    private def searchButton = $('.icon.icon-search')
    private def searchInputField = $('[placeholder="search housing"]')
    private def sortDropdown = $('.search-sort .dropdown-list')
    private def sortDropdownItems = $$('.search-sort .dropdown-item.mode')
    private def housingItemPrices = $$('.result-meta .result-price')


    @Override
    void expectPageElements() {
        searchInputField.shouldBe(visible)
        searchButton.shouldBe(visible)
        sortDropdown.shouldBe(visible)
    }

    void openSortDropdown() {
        sortDropdown.click()
    }

    void useSearch() {
        searchInputField.sendKeys("Helsinki")
        searchButton.click()
    }

    List getSortDropdownValues() {
        sortDropdownItems.collect { element -> element.getText() }
    }

    void selectSortItem(String sortItem) {
        sortDropdownItems.find { item -> (item.getText() == sortItem) }.click()
    }

    List getSortableListForSpecificCurrency(String currency) {
        getPriceListForSpecificCurrency(currency)
                .findAll()
                .collect {
            it -> it.split("\\D").last() as Long
        }
    }

    List getPriceListForSpecificCurrency(String currency) {
        allCurrencyPriceList.collect { it -> it.find("$currency\\d+") }
    }

    List getAllCurrencyPriceList() {
        housingItemPrices.collect { element -> element.getText() }
    }
}
