package com.scout.web.at.pages

abstract class Page {

    abstract void expectPageElements()
}
