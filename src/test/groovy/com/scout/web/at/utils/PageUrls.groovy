package com.scout.web.at.utils

class PageUrls {

    private static final String HOST = "http://helsinki.craiglist.com"
    private static final String EN_LANGUAGE = "?lang=en&cc=gb"


    static String getMainPageUrl(String lang = "EN") {
        switch (lang) {
            case "EN": return HOST + EN_LANGUAGE
            default: return HOST
        }
    }

    static String getHousingPageUrl() {
        HOST + "/d/housing/search/hhh"
    }
}
