package com.scout.web.at.steps

import com.scout.web.at.pages.HousingPage
import cucumber.api.java.en.Given

import javax.inject.Inject

import static org.hamcrest.CoreMatchers.equalTo
import static org.hamcrest.MatcherAssert.assertThat

class HousingPageStepdefs {

    @Inject
    HousingPage housingPage

    @Given('^user opens sort dropdown$')
    void user_opens_sort_dropdown() {
        housingPage.openSortDropdown()
    }

    @Given('^sort dropwdown values are:$')
    void check_dropdown_values(List expectedDropdownValues) {
        List actualDropdownValues = housingPage.sortDropdownValues
        assertThat("Dropdown values do not match", actualDropdownValues, equalTo(expectedDropdownValues))
    }

    @Given('^user uses search$')
    void user_uses_search() {
        housingPage.useSearch()
    }

    @Given('^user sorts by (.*)$')
    void user_sorts_by(String sortItem) {
        housingPage.selectSortItem(sortItem)
    }

    @Given('^housing items for given (?:currency|currencies) (?:is|are) in (ascending|descending) order:$')
    void check_price_sorted_order(String sortOrder, List currencies) {
        for (currency in currencies) {
            List actualOrdering = housingPage.getSortableListForSpecificCurrency(currency.toString())

            if (sortOrder == "ascending") {
                assertThat("Prices for currency: $currency are not in ascending order",
                        actualOrdering, equalTo(actualOrdering.sort(false) { it }))
            } else {
                assertThat("Prices for currency: $currency are not in descending order",
                        actualOrdering, equalTo(actualOrdering.sort(false) { -it }))
            }
        }
    }


}
